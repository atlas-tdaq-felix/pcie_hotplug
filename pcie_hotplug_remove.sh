#!/bin/bash

#MJ: It seems we can remove the FLX card and rescan the bus without unloading / loading the flx driver
echo "+----------------------------------------------------+"
echo "| Removing FLX and PEX endpoints before F/W update   |"
echo "+----------------------------------------------------+"

echo "Checking if script is run by root ..."
if [ "$(id -u)" != "0" ]; then  
  echo "ERROR: This script must be run as root"  
  exit 1;
fi

KEEP_GOING=1
PCI_DEVICES_FOUND=0
PEX_DEVICES_FOUND=0

echo ""
echo "Searching for FLX-709, FLX-710, FLX-711, FLX-712 devices ..."
while [ $KEEP_GOING != "0" ]; do
  if [ "$(/sbin/lspci -n | grep -e "10ee:703\|10dc:042")" != "" ]; then  
    PCI_SLOT=`/sbin/lspci -n | grep -e "10ee:703\|10dc:042" | head -n 1 | awk '{print $1}'`
    let PCI_DEVICES_FOUND=PCI_DEVICES_FOUND+1
    echo PCI_SLOT of FLX-7xx number $PCI_DEVICES_FOUND is $PCI_SLOT
    echo "Removing this device ..."
    echo 1 > /sys/bus/pci/devices/0000:$PCI_SLOT/remove
    echo "FLX-7xx removed ..."
    echo ""
  else
    KEEP_GOING=0
  fi
done
echo $PCI_DEVICES_FOUND FLX devices found and removed

KEEP_GOING=1
echo ""
echo "Searching for PEX devices ..."
while [ $KEEP_GOING != "0" ]; do
  if [ "$(/sbin/lspci -n | grep 10b5:8732)" != "" ]; then  
    PCI_SLOT=`/sbin/lspci -n | grep 10b5:8732 | tail -n 1 | awk '{print $1}'`
    let PEX_DEVICES_FOUND=PEX_DEVICES_FOUND+1
    echo PCI_SLOT of PEX number $PEX_DEVICES_FOUND is $PCI_SLOT
    echo "Removing this device ..."
    echo 1 > /sys/bus/pci/devices/0000:$PCI_SLOT/remove
    echo "PEX removed ..."
    echo ""
  else
    KEEP_GOING=0
  fi
done
echo $PEX_DEVICES_FOUND PEX devices found and removed


#Next  lines just for debugging
echo "lspci check"
/sbin/lspci -n | grep -e "10ee:703\|10dc:042"
/sbin/lspci -n | grep 8732
# pex count
ret=`/sbin/lspci -n | grep 8732|wc -l`
exit $ret
