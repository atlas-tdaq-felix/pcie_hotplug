# pcie_hotplug

This directory contains two supported file: pcie_hotplug_remove.sh and pcie_hotplug_rescan.ch
The package is maintained by M. Joos

The script are able to iterate the PCIe devices and let the OS recognize them without
rebooting. However use it at your own risk, the behaviour may not be as good
as a real reboot. Especially if the BAR content has been changed in the 
firmware.

NOTE: The scripts have been tested on the CERN testbed with two cards installed.
pcie_hotplug_rescan.sh restarts the tdaq-driver service not necessarily present in all testbeds.
 
