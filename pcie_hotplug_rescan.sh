#!/bin/bash

#MJ: It seems we can remove the FLX card and rescan the bus without unloading / loading the flx driver
echo "+----------------------------------------------------+"
echo "| Re-activating FLX cards after F/W update           |"
echo "+----------------------------------------------------+"

echo "Checking if script is run by root ..."
if [ "$(id -u)" != "0" ]; then  
  echo "ERROR: This script must be run as root"  
  exit 1;
fi

echo "Re-enumerate PCI ..."
echo 1 > /sys/bus/pci/rescan

PCI_DEVICES_FOUND_AGAIN=`/sbin/lspci -n | grep -e "10ee:703\|10dc:042" | wc -l`
echo $PCI_DEVICES_FOUND_AGAIN FLX devices found after PCIe rescan

PEX_DEVICES_FOUND_AGAIN=`/sbin/lspci -n | grep 10b5:8732 | wc -l`
echo $PEX_DEVICES_FOUND_AGAIN PEX devices found after PCIe rescan

echo "Restarting tdaq-driver.service..."
echo 1 > systemctl restart tdaq-driver.service

dmesg | tail -n 33
echo "That's it"  
exit 1;

