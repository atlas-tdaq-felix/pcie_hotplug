#!/bin/bash

#MJ: It seems we can remove the FLX card and rescan the bus without unloading / loading the flx driver
echo "+----------------------------------------------------+"
echo "| Re-activating FLX cards after F/W update           |"
echo "+----------------------------------------------------+"

echo "Checking if script is run by root ..."
if [ "$(id -u)" != "0" ]; then  
  echo "ERROR: This script must be run as root"  
  exit 1;
fi

KEEP_GOING=1
PCI_DEVICES_FOUND=0

echo ""
echo "Searching for FLX-709, FLX-711 anf FLX-712 devices ..."

/sbin/lspci -n | grep -c -e "10ee:703\|10dc:042"

while [ $KEEP_GOING != "0" ]; do
  if [ "$(/sbin/lspci -n | grep -e "10ee:703\|10dc:042")" != "" ]; then  
    PCI_SLOT=`/sbin/lspci -n | grep -e "10ee:703\|10dc:042" | head -n 1 | awk '{print $1}'`
    let PCI_DEVICES_FOUND=PCI_DEVICES_FOUND+1
    echo PCI_SLOT of FLX-7xx number $PCI_DEVICES_FOUND is $PCI_SLOT
    echo "Removing this device ..."
    echo 1 > /sys/bus/pci/devices/0000:$PCI_SLOT/remove
    echo "FLX-7xx removed ..."
    echo ""
  else
    KEEP_GOING=0
  fi
done

echo $PCI_DEVICES_FOUND FLX devices found and removed
echo ""

#Next two lines just for debugging
#echo "lspci check"
#/sbin/lspci -n | grep 10ee

echo "Re-enumerate PCI ..."
echo 1 > /sys/bus/pci/rescan

PCI_DEVICES_FOUND_AGAIN=`/sbin/lspci -n | grep -e "10ee:703\|10dc:042" | wc -l`
echo $PCI_DEVICES_FOUND_AGAIN FLX devices found after PCIe rescan

if [ $PCI_DEVICES_FOUND != $PCI_DEVICES_FOUND_AGAIN ]; then  
  echo "ERROR: Number of devices does not match"
fi

echo ""

dmesg | tail -n 33
echo "That's it"  
exit 1;


#Unused code.....
#echo "Check if driver is loaded ..."
#if [ "$(lsmod | grep flx)" != "" ]; then
#    echo "Driver is on ..."
#    echo "Turn off"
#    rmmod flx
#fi
#echo "Driver is off..."
#echo ""
###
#echo "Loading flx driver ..."
#modprobe flx
#echo "Check if loaded ..."
#if [ "$(lsmod | grep flx)" == "" ]; then
#    echo "ERROR: Driver is not loaded ..."
#    exit 1
#fi
